const canvas = document.getElementById('canvas');
const planetControls = document.querySelector('#controlorb ul')
const viewType = document.querySelector('#control input[type="checkbox"]')
let switchview = false

const planetXOrbitRadius = [90,170.2,240,350.9,480,590.5,660.5,700.1]
const planetRadius = [10,20,25,17,40,35,30,28]

var Mercury = new ConstructPlanet(90,30,10,"#d1d1d1",0.05)
var Venus = new ConstructPlanet(170.2,80,20,"#964b05",0.03)
var Earth = new ConstructPlanet(240,100,25,"#0960bd",0.02)
var Mars = new ConstructPlanet(350.9,150,17,"#bd2a09",0.01)
var Jupiter = new ConstructPlanet(480,200,40,"#d19213",0.007)
var Saturn = new ConstructPlanet(590.5,225,35,"#3c689e",0.004)
var Uranus = new ConstructPlanet(660.5,250,30,"#3c879e",0.0015)
var Neptune = new ConstructPlanet(700.1,300.5,28,"#377ebf",0.0005)

viewType.addEventListener('click',()=>{
    switchview = viewType.checked;
    if(switchview){
        PlanetArray.forEach(planet => {
            planet.radiusX = planet.radiuxY
            planet.planetSize = 10
        })
    }
    else{
        PlanetArray.forEach((planet,el) => {
            
            planet.radiusX = planetXOrbitRadius[el]
            planet.planetSize = planetRadius[el]
        })
    }
})

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let C_WIDTH = canvas.width;
let C_HEIGHT = canvas.height;
let CENTER_X = C_WIDTH*0.5
let CENTER_Y = C_HEIGHT*0.5
let starCount = 800
var TIME = 0

const context = canvas.getContext('2d')
const planetContext = canvas.getContext('2d')

// twinkling star background is set
//star object declaration

//reference taken by https://codepen.io/Jenbo/pen/pgmZwB?__cf_chl_jschl_tk__=3514fcb2ba9d1cbbc6f3c777f8849248c7925bbf-1597121856-0-Ac9nEzFqs-oSpySSoq94EDAeNKkUC1hV-N2ZjFMoE4V2n_KfaSwUUnyCQWn3Fx6bn81ZFBf9h9WhBHe_GgpXiQjqJrCT7g1Vd3uCSruBggmhinEWHLVhupnlM0gZxm_iaaeTY71oIxMKyXlspxW6SBnvjm6YYwdOlifH8oNZPGd96UvRSDqAaErPaM-8QGrnuZEuPhWKKQqxefPWE2n5ESFLpiCsI79o2UGUeoX96E575VKNuo41mMm43oX28W0wTQJNo37zD3jasF48EyL8sv8QnCVf_oEH17UlrA5ixTGBxkg9Bbf34qtE33ngBBY6-pYVQGq5ipv-ZxxY_1Q1CGpGfDhE8q2QxwWrTNPEQrmm
function Background(x,y,r,color){
    this.x = x;
    this.y = y;
    this.r = r;
    this.rChange = 0.015;
    this.color = color;
}

Background.prototype = {
    constructor: Background,
    render: function(){
      context.beginPath();
      context.arc(this.x, this.y, this.r, 0, 2*Math.PI, false);
      context.fillStyle = this.color;
      context.fill();
    },
    update: function(){
      
       if (this.r > 2 || this.r < .8){
           this.rChange = - this.rChange;
       }
       this.r += this.rChange;
    }
}







function StarrandomColor(){
    var arrColors = ["ffffff30", "ffecd330" , "bfcfff30","ffffff10","e6e7e8"];
    return '#'+arrColors[Math.floor((Math.random()*4))];
}

//creating 500 stars at random locations in the canvas
var arrStars = [];
for(i = 0; i < starCount; i++){
    var randX = Math.floor((Math.random()*C_WIDTH)+1);
    var randY = Math.floor((Math.random()*C_HEIGHT)+1);
    var randR = Math.random() * 0.5 + .5;
    
    var star = new Background(randX, randY, randR, StarrandomColor());
    arrStars.push(star);
}
function updateStars(){
  for(i = 0; i < arrStars.length; i ++){
    arrStars[i].update();
  }
}
// reference taken from https://www.html5canvastutorials.com/advanced/html5-canvas-animated-solar-system/
// Sun object creation 
function Sun(){
    planetContext.beginPath()
    planetContext.fillStyle ="yellow"
    planetContext.arc(CENTER_X, CENTER_Y, 50, 0, Math.PI * 2, true);
    planetContext.fill()
    planetContext.stroke()
    
}
function ConstructPlanet(radiusX,radiuxY,planetSize,planetColor,orbitalVelocity,orbitSelected){
    this.radiusX = radiusX
    this.radiuxY = radiuxY
    this.planetSize = planetSize
    this.planetColor = planetColor
    this.orbitalVelocity = orbitalVelocity/2
    this.orbitSelected = orbitSelected

    

    this.render = function () {
        
        context.beginPath();
        context.ellipse(C_WIDTH*0.5, C_HEIGHT*0.5, this.radiusX, this.radiuxY, 0, 0, 2 * Math.PI);
        if (!this.orbitSelected) {
            context.setLineDash([5, 10]);
            context.strokeStyle  = "gray";
        }
        else{
            context.setLineDash([5, 4]);
            context.strokeStyle  = "pink";
        }
        context.stroke();
        context.beginPath()
        context.arc(CENTER_X-this.radiusX*Math.cos(TIME*this.orbitalVelocity), CENTER_Y-this.radiuxY*Math.sin(TIME*this.orbitalVelocity), this.planetSize, 0, Math.PI * 2, true);
        context.fillStyle = this.planetColor
        context.fill()
    }

    this.selectPath = function(){
        this.orbitSelected = true
    }

}






const PlanetArray = [Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune]


function animate(){
    updateStars();
    TIME+=1
    context.clearRect(0,0,C_WIDTH,C_HEIGHT);
    

    Sun()

    PlanetArray.forEach(planet => {
        planet.render()
    });

    
   
    for(var i = 0; i < arrStars.length; i++){
      arrStars[i].render();
    }
    requestAnimationFrame(animate);
}

animate();


planetControls.addEventListener('click',(e)=>{
    const planet = e.target
    PlanetArray.forEach(el=>el.orbitSelected=false)
    window[planet.dataset.name].orbitSelected = true
    
})

